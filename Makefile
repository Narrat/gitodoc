# Simple Makefile

# Compiler
CC=clang
#CC=gcc

TARGET = gitodoc # inserted with $@

# Flags
CFLAGS	= -Wall -Wextra -Wconversion
LDFLAGS	+= -Wl,-z,now -Wl,-z,relro -pie
LIBS	+= $(shell pkg-config --cflags --libs libgit2)
LIBS	+= $(shell pkg-config --cflags --libs libpcre2-8)

all: gitodoc

debug: CFLAGS += -g
debug: LDFLAGS += -g
debug: gitodoc

gitodoc: gitodoc.o item_read.o item_write.o utilities.o git_handling.o
	$(CC) $(CFLAGS) ${LIBS} $^ -o $@

gitodoc.o: gitodoc.c
	$(CC) $(CFLAGS) -c gitodoc.c

item_read.o: item_read.c item_read.h
	$(CC) $(CFLAGS) -c item_read.c

item_write.o: item_write.c item_write.h
	$(CC) $(CFLAGS) -c item_write.c

utilities.o: utilities.c utilities.h
	$(CC) $(CFLAGS) -c utilities.c

git_handling.o: git_handling.c git_handling.h
	$(CC) $(CFLAGS) -c git_handling.c

clean:
	rm *.o gitodoc
