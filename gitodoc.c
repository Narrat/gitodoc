/*
 * gitodoc main file
 */


#define _GNU_SOURCE /* Needed, because of GNU getopt_long and not the POSIX one */
#include <stdio.h>
#include <getopt.h>     /* Just options, no arguments (although some options have arguments :D) */
/* with ruby those can also be used as arguments --> "--list" allows "list" and "l" */

#include "item_read.h"
#include "item_write.h"
#include "git_handling.h"


/* Option parsing stuff - GNU getopt (for optional arguments) */
static const char optstring[] = "hl:e:p:n::f:d:b:cor";
static const struct option options_long[] = {
    /* name             has_arg             flag    val */
    { "help",           no_argument,        NULL,   'h' },
    { "list",           required_argument,  NULL,   'l' },
    { "edit",           required_argument,  NULL,   'e' },
    { "print",          required_argument,  NULL,   'p' },
    { "new",            optional_argument,  NULL,   'n' },
    { "new-from-file",  required_argument,  NULL,   'f' },
    { "delete",         required_argument,  NULL,   'd' },
    { "body",           required_argument,  NULL,   'b' },
    { "cron",           no_argument,        NULL,   'c' },
    { "cron-outdated",  no_argument,        NULL,   'o' },
    { "count",          no_argument,        NULL,   'C' },
    { "raw",            no_argument,        NULL,   'r' },
    { "rotting",        optional_argument,  NULL,   'R' },
    { 0, 0, 0, 0 }
};

void usage();

int main(int argc, char *argv[]) {
    settings gitodoc = {"", "", FALSE, NONE, 0};
    char old_cwd[PATH_MAX] = "", file_path[PATH_MAX] = "";
    int option, to_commit=0, print_done=-1, ret=0, err=0;
    size_t dsize = LEN_DATA;
    void *pos = NULL;
    long nmb_of_new=1, rot_months=3;
    item_t *item_list_begin = NULL;
    item_t *item_list_current = NULL;
    opt_input_t option_arg = {"", NOIN};

    /* Environment check */
    ret = get_data_dir(gitodoc.data_dir);
    if ( ret == -1 ) {
        fprintf(stderr, "gitodoc main: Error getting data dir '%s': %s\n", gitodoc.data_dir, strerror(errno));
        return -1;
    }
    get_editor(gitodoc.editor);
    gitodoc.important = get_important();
    gitodoc.how_to_print = NONE;

    /* Setup: Save cwd, change to data_dir and check for repo; On error bail */
    if ( getcwd(old_cwd, sizeof(old_cwd)) == NULL ) {
        perror("getcwd() error");
    }
    ret = chdir(gitodoc.data_dir);
    if ( ret == -1 ) {
        fprintf(stderr, "gitodoc main: Error changing to '%s': %s\n", gitodoc.data_dir, strerror(errno));
        return -1;
    }
    if ( check_for_repo(".") != 0 ) {
        return -1;
    }

    /* option parsing */
    for ( int i = 0; ; i++ ) {
        /*
         * Structure of gitodoc.arguments
         * Variable of type long to store 4 int types via bit-shifting
         * 1) Input-related (<< 24)
         *    0: NOIN (No input)
         *    1: DIRECT (use input directly w/o matching)
         *    2: Determine input (if there is input and if REGEX or SINGLE)
         *    3: REGEXF (w/o identifier; searches complete file)
         * 2) Read/Write operations (<< 16)
         *    1: Get items
         *    2: Get items for --rotting
         *    3: Edit item
         *    4: New item(s)
         *    5: New item with filepath (candidate for merging with 4)
         *    6: Delete item
         * 3) Additional procedere (<< 8)
         *    1: Commit changes
         *    2: Sort linked list
         *    3: Count items
         * 4) Output-related (<< 0) (currently unused)
         * Special case: 0 == call of --help
         */
        option = getopt_long(argc, argv, optstring, options_long, NULL);
        if ( option == -1 && i != 0 ) {
            break;
        }
        switch (option) {
            case 'h':
                gitodoc.arguments = ((0 << 24) | (99 << 16) | (99 << 8) | 0);
                break;
            case 'l':
                gitodoc.arguments = ((2 << 24) | (1 << 16) | (2 << 8) | 0);
                gitodoc.how_to_print = PRETTY;
                break;
            case 'e':
                gitodoc.arguments = ((2 << 24) | (3 << 16) | (1 << 8) | 0);
                break;
            case 'p':
                gitodoc.arguments = ((2 << 24) | (1 << 16) | (0 << 8) | 0);
                gitodoc.how_to_print = SPECIFIC;
                break;
            case 'n':
                gitodoc.arguments = ((1 << 24) | (4 << 16) | (1 << 8) | 0);
                break;
            case 'f':
                gitodoc.arguments = ((1 << 24) | (5 << 16) | (1 << 8) | 0);
                break;
            case 'd':
                gitodoc.arguments = ((2 << 24) | (6 << 16) | (1 << 8) | 0);
                break;
            case 'b':
                gitodoc.arguments = ((3 << 24) | (1 << 16) | (2 << 8) | 0);
                gitodoc.how_to_print = SPECIFIC;
                break;
            case 'c':
                gitodoc.arguments = ((0 << 24) | (1 << 16) | (2 << 8) | 0);
                gitodoc.how_to_print = CRON_DC;
                break;
            case 'o':
                gitodoc.arguments = ((0 << 24) | (1 << 16) | (2 << 8) | 0);
                gitodoc.how_to_print = CRON_D;
                break;
            case 'C':
                gitodoc.arguments = ((0 << 24) | (1 << 16) | (3 << 8) | 0);
                break;
            case 'r':
                /* Don't do pretty printing. This option is meant for scripting */
                gitodoc.arguments = ((0 << 24) | (1 << 16) | (0 << 8) | 0);
                gitodoc.how_to_print = RAW;
                break;
            case 'R': /* --rotting */
                gitodoc.arguments = ((0 << 24) | (2 << 16) | (2 << 8) | 0);
                gitodoc.how_to_print = PRETTY;
                break;
            case '?': /* invalid argument/option */
                gitodoc.arguments = ((0 << 24) | (99 << 16) | (99 << 8) | 0);
                break;
            default: /* no argument/options */
                gitodoc.arguments = ((0 << 24) | (1 << 16) | (2 << 8) | 0);
                gitodoc.how_to_print = PRETTY;
                break;
        }
        /* Put optarg into separate variable to avoid issues when mem-shifting the former */
        if ( optarg != NULL ) {
            pos = memccpy(option_arg.value, optarg, '\0', dsize);
            if (pos == NULL) {
                fprintf(stderr, "getopt_long: Input '%s' exceeded allowed size of %d bytes + NULL... Skipping the input\n", optarg, LEN_DATA-1);
                continue;
            }
        } else {
            pos = memccpy(option_arg.value, "", '\0', dsize);
        }

        /* Input related */
        switch ( gitodoc.arguments >> 24 ) {
            case 0: // No input
                option_arg.input_type = NOIN;
                break;
            case 1: // Do nothing
                option_arg.input_type = DIRECT;
                break;
            case 2: // Determine input
                option_arg.input_type = input_mapping(option_arg.value);
                if ( option_arg.input_type == INVALID ) {
                    gitodoc.arguments -= (1 << 16);
                }
                break;
            case 3: // Full body REGEX
                option_arg.input_type = REGEXF;
                break;
        }

        /* Read/Write related */
        switch ( gitodoc.arguments >> 16 & 0xff ) {
            case 1: // Get Items (value/type & -1)
                get_items(&item_list_begin, &option_arg, -1);
                break;
            case 2: // Get Items (type & optarg)
                if ( optarg != NULL ) {
                    err = str_to_long(option_arg.value, &rot_months);
                }
                if ( err == 0) {
                    get_items(&item_list_begin, &option_arg, rot_months);
                }
                break;
            case 3: // Edit Item
                ret = edit_item(gitodoc, option_arg.value);
                break;
            case 4: // New item
                if ( optarg != NULL ) {
                    ret = str_to_long(option_arg.value, &nmb_of_new);
                }
                for (int i=1; i<=nmb_of_new && ret == 0; i++) {
                    ret = new_item(gitodoc, NULL);
                    if ( ret == 0 ) {
                        to_commit++;
                    }
                }
                break;
            case 5: // New item with filepath
                ret = build_filepath(file_path, old_cwd, option_arg.value);
                if (ret == 0) {
                    ret = new_item(gitodoc, file_path);
                }
                break;
            case 6: // Delete item
                ret = delete_item(option_arg);
                break;
        }
        /* No items -> No sort needed */
        if ( item_list_begin == NULL && ((gitodoc.arguments >> 16 & 0xff) <= 2) ) {
            gitodoc.arguments -= (2 << 8);
        }
        /* If something went wrong while working through Write related stuff -> remove the to-commit flag */
        if ( ret != 0 ) {
            gitodoc.arguments -= (1 << 8);
        }
    }

    /* Output related arguments */
    switch ( gitodoc.arguments >> 8 & 0xff ) {
        case 1: /* to Commit or not */
            commit_changes();
            break;
        case 2: /* sort items */
            item_list_begin = sort_items(item_list_begin);
            break;
        case 3: /* count items */
            count_items(item_list_begin);
            break;
        case 99: /* show help */
            usage();
            break;
    }

    /* How and what items are presented */
    switch (gitodoc.how_to_print) {
        case NONE:
            break;
        case PRETTY:
            print_complete_list(gitodoc, item_list_begin);
            break;
        case RAW:
            print_complete_list_raw(item_list_begin);
            break;
        case SPECIFIC:
            print_item_content(item_list_begin);
            break;
        case CRON_DC:
            for ( Classification i=DEAD; i<=CLOSE; i++ ) {
                print_done = print_cron_items(item_list_begin, i, print_done);
            }
            break;
        case CRON_D:
            print_done = print_cron_items(item_list_begin, DEAD, -1);
            break;
    }

    /* Free linked list */
    while ((item_list_current = item_list_begin) != NULL) {
        item_list_begin = item_list_begin->next;
        free(item_list_current);
    }

    return 0;
}

void usage()
{
    fprintf(stderr, "gitodoc [options]\n\n");
    fprintf(stderr, "Simple ToDo manager.\n\n");
    fprintf(stderr, "    -h, --help               Show this help\n");
    fprintf(stderr, "    -l, --list=ID            List matching ID\n");
    fprintf(stderr, "    -e, --edit=ID            Edit matching ID\n");
    fprintf(stderr, "    -p, --print=ID           Print matching ID\n");
    fprintf(stderr, "    -n, --new                Create new task \n");
    fprintf(stderr, "    -f, --new-from-file      Create task from file\n");
    fprintf(stderr, "    -d, --delete=ID          Delete matching ID\n");
    fprintf(stderr, "    -b, --body=TERM          Search for TERM in the bodies\n");
    fprintf(stderr, "    -c, --cron               Show outdated or close to tasks\n");
    fprintf(stderr, "    -o, --cron-outdated      Show outdated tasks\n");
    fprintf(stderr, "        --count              Count tasks\n");
    fprintf(stderr, "        --raw                Don't do pretty printing\n");
    fprintf(stderr, "        --rotting[=m]        List items without when that are rotting for # months\n");
}
