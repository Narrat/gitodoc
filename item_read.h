/*
 * Functions needed for reading the files
 */

#ifndef ITEM_READ_H
#define ITEM_READ_H

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "git_handling.h"
#include "utilities.h"


/*
 * Color definitions
 */
#define ANSI_NORMAL_TEXT            "\x1b[0m"
#define ANSI_TEXT_BOLD              "\x1b[1m"
#define ANSI_COLOR_RED              "\x1b[31m"
#define ANSI_COLOR_GREEN            "\x1b[32m"
#define ANSI_COLOR_YELLOW           "\x1b[33m"
#define ANSI_COLOR_BLUE             "\x1b[34m"
#define ANSI_COLOR_MAGENTA          "\x1b[35m"
#define ANSI_COLOR_CYAN             "\x1b[36m"


void get_item(item_t **, char *);
void get_items(item_t **, opt_input_t *, long);
void search_item(item_t **, char *, char *);
void count_items(item_t *);
void print_complete_list_raw(item_t *);
void print_header();
void print_separator();
void print_item(item_t *);
void print_complete_list(settings, item_t *);
int print_cron_items(item_t *, Classification, int);
void print_item_content(item_t *);

#endif
