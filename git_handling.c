/*
 * Functions for managing the gitodoc repository
 */

#include "git_handling.h"


void check_error(int error_code, const char *action)
{
    /*
     * Git error code checking
     */
    const git_error *error = git_error_last();
    if ( !error_code ) {
        return;
    }

    printf("%s: Error %d - %s\n", action, error_code, (error && error->message) ? error->message : "???");
    return;
}

int check_for_repo(char *path)
{
    git_repository *repo;
    int error = 0;

    git_libgit2_init();
    if ( (error = git_repository_open(&repo, path)) != 0 ) {
        check_error(error, "check_for_repo");
    }
    if ( error == 0 ) {
        git_repository_free(repo);
    }
    git_libgit2_shutdown();

    return error;
}

void check_age(Age *age_class, char *file_name, long months)
{
    /*
     * Walk through the commit history to search for changes in an item.
     * The file in question will be taken from current commit and its parent commit
     * and if the respective file OID changes between those two, the file was changed in commit.
     *
     * Something equivalent to: git log '--pretty=format:%ct' -n 1 -- "#{i.filename}"
     *
     * Issues: performance compared to the git log call is worse.
     * Depends on the following factors:
     * - Number of commits in the history
     * - how long each item is sitting in the repo
     * - use of when:
     * - # of items
     *
     * To document some numbers:
     * - main repo: 0,5s -> 1,5s (long history, relatively fresh items, moderate use of when, lot of files)
     * - test repo: 5s -> 25s (moderate history, really long untouched items, infrequent use of when, little # of files)
     */
    FILE *fp, *head_fileptr;
    char head_filepath[PATH_MAX], head_rev[41], *pos;
    time_t commit_time;
    int error=0, age=-1;
    size_t dest_end = (size_t)head_filepath + sizeof(head_filepath);
    double diff_time=0;
    git_repository *repo;
    git_oid oid;
    git_revwalk *walker;
    git_commit *commit, *parent;
    git_buf oid_obj1 = {0}, oid_obj2 = {0};

    /* Check file for when entry */
    fp = fopen(file_name, "r");
    if ( fp != NULL ) {
        age = search_file(fp, "^[d,w][e,h].[d,n]:");
        fclose(fp);
    } else {
        fprintf(stderr, "check_age: Error opening file '%s': %s\n", file_name, strerror(errno));
        /* No need to return; still possible to check the git history */
    }
    if ( age == 0 ) {
        *age_class = HAS_DATE;
        return;
    }

    git_libgit2_init();

    /* Open repo at CWD */
    if ( (error = git_repository_open(&repo, REPO)) != 0 ) {
        check_error(error, "check_age");
        *age_class = UNCLASSIFIED;
        goto git_shutdown;
    }

    /* Read HEAD on master */
    pos = memccpy(head_filepath, REPO, '\0', PATH_MAX);
    pos = memccpy(pos-1, "/refs/heads/master", '\0', dest_end-(size_t)pos+1);
    if ( pos == NULL ) {
        fprintf(stderr, "check_age: path to refs/head/master isn't a string\n");
        goto git_shutdown;
    }

    if ( (head_fileptr = fopen(head_filepath, "r")) == NULL ) {
        fprintf(stderr, "check_age: Error opening '%s'\n", head_filepath);
        *age_class = UNCLASSIFIED;
        goto git_shutdown;
    }

    if ( fread(head_rev, 40, 1, head_fileptr) != 1 ) {
        fprintf(stderr, "check_age: Error reading from '%s'\n", head_filepath);
        fclose(head_fileptr);
        *age_class = UNCLASSIFIED;
        goto git_shutdown;
    }

    fclose(head_fileptr);

    /* Prepare revision walker */
    if ( git_oid_fromstr(&oid, head_rev) != 0 ) {
        fprintf(stderr, "Invalid git object: '%s'\n", head_rev);
        *age_class = UNCLASSIFIED;
        goto git_shutdown;
    }
    git_revwalk_new(&walker, repo);
    git_revwalk_sorting(walker, GIT_SORT_TOPOLOGICAL);
    git_revwalk_push(walker, &oid);

    /* Walk through commits and stop where file changed */
    while ( git_revwalk_next(&oid, walker) == 0 ) {
        if ( (error = git_commit_lookup(&commit, repo, &oid)) != 0 ) {
            check_error(error, "check_age");
            *age_class = UNCLASSIFIED;
            goto git_revwalk;
        }
        if ( (error = git_get_file_oid(&oid_obj1, repo, file_name, commit)) != 0 ) {
            check_error(error, "check_age");
            *age_class = UNCLASSIFIED;
            goto git_commit;
        }

        /* If parent commit is available, retrieve file OID */
        if ( git_commit_parentcount(commit) > 0 ) {
            if ( (error = git_commit_parent(&parent, commit, 0)) != 0 ) {
                check_error(error, "check_age");
                *age_class = UNCLASSIFIED;
                goto git_buffer;
            }
            if ( (error = git_get_file_oid(&oid_obj2, repo, file_name, parent)) != 0 ) {
                check_error(error, "check_age");
                *age_class = UNCLASSIFIED;
                goto git_buffer;
            }
        }

        /* If commit and parent file OID is different, the change occured in commit */
        if ( strncmp(oid_obj1.ptr, oid_obj2.ptr, oid_obj1.size) != 0 ) {
            break;
        }
    }

    /* When fitting commit found calc time diff */
    commit_time = git_commit_time(commit);
    diff_time = time_delta(commit_time);
    age = abs((int)(diff_time/(24*30)));

    if ( age < months ) {
        *age_class = NOT_DORMANT;
    } else {
        *age_class = DORMANT;
    }

    /* Cleanup */
git_buffer:
    git_buf_dispose(&oid_obj1);
    git_buf_dispose(&oid_obj2);
git_commit:
    git_commit_free(commit);
    git_commit_free(parent);
git_revwalk:
    git_revwalk_free(walker);
git_shutdown:
    git_libgit2_shutdown();
}

void commit_changes()
{
    /*
     * The libgit2 equivalent of
     *     $ git add -A .
     *     $ git commit -m "Auto-Commit by gitodoc on $(hostname)"
     * is done with this function
     */
    char commit_message[LEN_MSGS] = "Auto-Commit by gitodoc on ";
    char hostname[HOST_NAME_MAX+1] = "";
    git_repository *repo;
    git_signature *authcom = NULL;
    git_object *git_obj = NULL;
    git_commit *parent = NULL;
    git_index *index = NULL;
    git_oid *parent_oid, tree_oid, new_commit_id;
    git_tree *tree = NULL;
    size_t remaining_len = LEN_MSGS - strlen(commit_message)+1;
    int error;

    /* Create commit message */
    error = gethostname(hostname, HOST_NAME_MAX+1);
    strncat(commit_message, hostname, remaining_len);

    /* Initialise libgit2 */
    git_libgit2_init();

    /* Open repo at cwd */
    if ( (error = git_repository_open(&repo, ".")) != 0 ) {
        check_error(error, "commit_changes");
        goto git_shutdown;
    }

    /* Create signature for commit */
    if ( (error = git_signature_default(&authcom, repo)) != 0 ) {
        check_error(error, "commit_changes");
        goto git_free_repo;
    }

    /* Last commit for parent information */
    error = git_revparse_single(&git_obj, repo, "HEAD");
    if (error == 0) { /* If one commit is found */
        parent_oid = (git_oid *)git_obj;
        if ( (error = git_commit_lookup(&parent, repo, parent_oid)) != 0 ) {
            check_error(error, "commit_changes");
            goto git_free_object;
        }
    }

    /* Get the git index */
    if ( (error = git_repository_index(&index, repo)) != 0 ) {
        check_error(error, "commit_changes");
        goto git_free_commit;
    }

    /* Add all files to the git index and write it to disk */
    if ( (error = git_index_add_all(index, NULL, 0, NULL, NULL)) != 0 ) {
        check_error(error, "commit_changes");
        goto git_free_commit;
    }

    if ( (error = git_index_write(index)) != 0 ) {
        check_error(error, "commit_changes");
        goto git_free_commit;
    }

    /* Write the index to a tree (needed for commit) */
    if ( (error = git_index_write_tree(&tree_oid, index)) != 0 ) {
        check_error(error, "commit_changes");
        goto git_free_commit;
    }

    /* Converts the OID of the tree object to Tree object */
    if ( (error = git_tree_lookup(&tree, repo, &tree_oid)) != 0 ) {
        check_error(error, "commit_changes");
        goto git_free_index;
    }

    /* Commit the added files */
    if ( (error = git_commit_create_v(
                &new_commit_id,
                repo,
                "HEAD",             /* name of ref to update */
                authcom,            /* author */
                authcom,            /* committer */
                "UTF-8",            /* message encoding */
                commit_message,     /* message */
                tree,               /* root tree */
                1,                  /* parent count */
                parent)) != 0 ) {
        check_error(error, "commit_changes");
        goto git_free_index;
    }
    printf("%s\n", commit_message);

    /* Free all of the libgit2 stuff */
git_free_index:
    git_index_free(index);
git_free_commit:
    git_commit_free(parent);
git_free_object:
    git_object_free(git_obj);
git_free_repo:
    git_repository_free(repo);
git_shutdown:
    git_libgit2_shutdown();
}

int git_get_file_oid(git_buf *oid_obj, git_repository *repo, char *filename, git_commit *cmt)
{
    /*
     * Return OID of file
     * In order to do that, get tree of supplied commit and from that tree the file entry by name.
     * With the entry the respective OID of the file can be retrieved.
     */

    git_tree *cmttree;
    const git_tree_entry *cmtentry;
    git_object *obj;
    int error;

    // Tree of commit
    if ( (error = git_commit_tree(&cmttree, cmt)) != 0 ) {
        check_error(error, "looking up tree from commit");
    }
    // Get entry of file in commit tree
    // Ignore NULL, as that may happen if a file was created but never changed
    if ( (cmtentry = git_tree_entry_byname(cmttree, filename)) != NULL ) {
        if ( (error = git_tree_entry_to_object(&obj, repo, cmtentry)) != 0 ) {
            check_error(error, "getting file entry from tree");
        }
    }

    // Get OID of file from commit tree
    if ( (git_object_short_id(oid_obj, obj)) != 0 ) {
        check_error(error, "getting OID of file");
    }

    git_object_free(obj);

    return 0;
}
