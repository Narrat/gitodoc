/*
 * Functions needed for reading the files
 */

#include "item_read.h"


void get_item(item_t **first_item, char *file_name)
{
    item_t *current_item = NULL;
    FILE *curr_file;
    int ret = -1;

    /* Check if such an entry already exists */
    ret = check_for_duplicates(*first_item, file_name);
    if (ret != 0) {
        return;
    }

    /* Try to open file and proceed if that succeeds */
    curr_file = fopen(file_name, "r");
    if ( curr_file != NULL ) {
        /* Initialize or append a linked list */
        if ( *first_item == NULL ) {
            *first_item = get_pos_to_append(*first_item);
            current_item = *first_item;
        } else {
            current_item = get_pos_to_append(*first_item);
        }
        memccpy(current_item->file_id, file_name, '\0', LEN_FNAME);

        /* Fill the current item_t struct */
        pattern_matching(current_item, curr_file);
    } else {
        fprintf(stderr, "get_item: Error opening file '%s': %s\n", file_name, strerror(errno));
        return;
    }

    fclose(curr_file);

    return;
}

void get_items(item_t **first_item, opt_input_t *item_ref, long rot_months)
{
    DIR *dir;
    struct dirent *dir_f;
    Age time=IGNORE;

    if ( item_ref->input_type < SINGLE ) {
        dir = opendir(".");
        if (dir) {
            while ( (dir_f = readdir(dir)) != NULL ) {
                if ( check_if_gitodoc_item(dir_f->d_name) == 0 ) {
                    if ( rot_months >= 0 ) {
                        check_age(&time, dir_f->d_name, rot_months);
                    }
                    if ( time == NOT_DORMANT || time == HAS_DATE ) {
                        continue;
                    }
                    if ( item_ref->input_type == NOIN ) {
                        get_item(first_item, dir_f->d_name);
                    } else {
                        search_item(first_item, dir_f->d_name, item_ref->value);
                    }
                }
            }
            closedir(dir);
            return;
        } else {
            /* Maybe use getcwd to make clear where it failed */
            fprintf(stderr, "Error opening directory: %s\n", strerror(errno));
            return;
        }
    } else {
        get_item(first_item, item_ref->value);
    }
}

void search_item(item_t **first_item, char *file_name, char *term)
{
    item_t *current_item = NULL;
    FILE *curr_file;
    int ret = -1;

    /* Check if such an entry already exists */
    ret = check_for_duplicates(*first_item, file_name);
    if (ret != 0) {
        return;
    }

    /* Try to open file and proceed if that succeeds */
    curr_file = fopen(file_name, "r");
    if ( curr_file != NULL ) {
        ret = search_file(curr_file, term);
        if ( ret == 0 ) {
            /* Case if a Match was found: Initialize or append a linked list */
            if ( *first_item == NULL ) {
                *first_item = get_pos_to_append(*first_item);
                current_item = *first_item;
            } else {
                current_item = get_pos_to_append(*first_item);
            }
            memccpy(current_item->file_id, file_name, '\0', LEN_FNAME);

            /* Fill the current item_t struct */
            /* Kinda unnecessary, but ID is needed */
            rewind(curr_file);
            pattern_matching(current_item, curr_file);
        }
    } else {
        fprintf(stderr, "Error opening the file %s: %s\n", file_name, strerror(errno));
        return;
    }
    fclose(curr_file);

    return;
}

void count_items(item_t *item)
{
    /*
     * Count tasks (dead, close, without, all/total)
     */
    unsigned int dead = 0, close = 0, wo = 0, all = 0;

    while (item != NULL) {
        all++;
        if ( time_status(item) == CLOSE ) {
            close++;
        }
        if ( time_status(item) == DEAD ) {
            dead++;
        }
        if ( strcmp(item->when, "2037-01-01 00:00:00") == 0 ) {
            wo++;
        }
        item = item->next;
    }
    printf("%d %d %d %d\n", dead, close, wo, all);
}

void print_complete_list_raw(item_t *item)
{
    while (item != NULL) {
        printf("%ld ", item->prio);
        printf("%s %ld %ld ", item->when, item->when_local, item->warn);
        printf("%d ", item->nocron);
        printf("%s %s\n", item->file_id, item->what);
        item = item->next;
    }
}

void print_header()
{
    printf(ANSI_TEXT_BOLD);
    printf(" S   Pri       Deadline        ID     Subject\n");
    printf(ANSI_NORMAL_TEXT);
}

void print_separator()
{
    printf(ANSI_TEXT_BOLD);
    printf("---+-----+------------------+------+------------------------------------------\n");
    printf(ANSI_NORMAL_TEXT);
}

void print_item(item_t *current_item)
{
    /*
     * Preparation of data:
     * Remove i from file name
     * colors and printing
     */
    int date_len = 16, date_str_len, date_counter;

    /* Warning handling; C for close, D for over; in red */
    if ( time_status(current_item) == NORMAL ) {
        printf(" %1s", " ");
    } else if ( time_status(current_item) == CLOSE ) {
        printf(ANSI_COLOR_RED);
        printf(" %1s", "C");
        printf(ANSI_NORMAL_TEXT);
    } else if ( time_status(current_item) == DEAD ) {
        printf(ANSI_TEXT_BOLD);
        printf(ANSI_COLOR_RED);
        printf(" %1s", "D");
        printf(ANSI_NORMAL_TEXT);
    }

    /* Color priority levels */
    if (current_item->prio <= -50) {
        printf(ANSI_TEXT_BOLD);
        printf(ANSI_COLOR_RED);
    } else if (current_item->prio > -50 && current_item->prio < 0) {
        printf(ANSI_COLOR_YELLOW);
    } else if (current_item->prio > 0 && current_item->prio < 50) {
        printf(ANSI_COLOR_GREEN);
    } else if (current_item->prio >= 50) {
        printf(ANSI_COLOR_BLUE);
    }

    printf(" | %3ld", current_item->prio);

    /* Check what should be printed in the Date field */
    if ( strcmp(current_item->when, "2037-01-01 00:00:00") != 0 ) { // or check on length, as seconds are additionally added with default
        printf(" | %s", current_item->when);
        date_str_len = (int)strlen(current_item->when);
        for ( date_counter=0; date_counter < date_len - date_str_len ; date_counter++ ) {
            printf(" ");
        }
    } else {
        printf(" | %16s", " ");
    }

    printf(" | %4ld", current_item->id);

    printf(" | %s\n", current_item->what);

    if (current_item->prio != 0) {
        printf(ANSI_NORMAL_TEXT);
    }
}

void print_complete_list(settings gitodoc, item_t *item)
{
    print_header();
    print_separator();

    while (item != NULL) {
        /* Check if an item should be printed */
        if ( item->prio <= 99 ||
             gitodoc.important == FALSE ||
             time_status(item) != NORMAL ) {
            print_item(item);
        }
        item = item->next;
    }
}

int print_cron_items(item_t *item, Classification to_print, int prev_success)
{
    /*
     * Depending on to_print it will either print
     * out-dated or close to deadline items
     */
    unsigned short item_there = 0;

    /* Check if a printable item even exists */
    while (item != NULL) {
        if ( time_status(item) == to_print ) {
            item_there++;
            break;
        }
        item = item->next;
    }

    if ( item_there == 0 ) {
        return -1;
    }

    /* Check if function did run previously */
    if ( prev_success == 0 && item_there > 0 ) {
        printf("\n\n");
    }

    /* Begin printing */
    if ( to_print == DEAD ) {
        printf("OUTDATED TASKS:\n");
    } else if ( to_print == CLOSE ) {
        printf("DEADLINE CLOSE:\n");
    }
    printf("===============\n\n\t");
    print_header();
    printf("\t");
    print_separator();

    while (item != NULL) {
        /* If the right Classification: print it */
        if ( time_status(item) == to_print && item->nocron == 0 ) {
            printf("\t");
            print_item(item);
        }

        item = item->next;
    }

    return 0;
}

void print_item_content(item_t *specific_item)
{
    FILE *item = NULL;
    char *line = NULL;
    size_t len = 0;

    /* Print the content of a specific ID */
    while ( specific_item != NULL ) {
        printf(ANSI_TEXT_BOLD);
        printf(" %ld\n------\n", specific_item->id);
        printf(ANSI_NORMAL_TEXT);

        item = fopen(specific_item->file_id, "r");
        if ( item == NULL ) {
            fprintf(stderr, "Error opening the file %s: %s\n", specific_item->file_id, strerror(errno));
            return;
        }
        while ( getline(&line, &len, item) != -1 ) {
            printf("%s", line);
        }
        fclose(item);

        printf("\n");
        specific_item = specific_item->next;
    }

    /* Some cleaning as getline allocated the buffer size */
    free(line);
}
