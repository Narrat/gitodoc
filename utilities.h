/*
 * Utility functions that are needed in one or more places
 */

#ifndef UTILITIES_H
#define UTILITIES_H

/* 8bit: ASCII */
#define PCRE2_CODE_UNIT_WIDTH 8

#include <ctype.h>
#include <dirent.h>
#include <limits.h> /* NAME_MAX, PATH_MAX and with POSIX extension HOST_NAME_MAX*/
#include <pcre2.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>


#ifndef NAME_MAX
    #define NAME_MAX _POSIX_NAME_MAX
#endif /* NAME_MAX */
#ifndef PATH_MAX
    #define PATH_MAX 4096
#endif /* PATH_MAX */
#ifndef HOST_NAME_MAX
    #define HOST_NAME_MAX 64
#endif /* HOST_NAME_MAX */
#define LEN_DATA 200
#define LEN_DATE 20 /* 2037-01-01 00:00:00 = 19 + '\0' */
#define LEN_FNAME 6


typedef struct item {
    char file_id[LEN_FNAME];  /* iXXXX */
    long id;                  /* XXXX */
    char what[LEN_DATA];      /* Summary */
    long prio;                /* -99 to 99; if omitted: 0 */
    char when[LEN_DATE];      /* YYYY-MM-DD HH:MM; if omitted: 2037-01-01 00:00:00 */
    time_t when_local;        /* Calendar time (sort and compare regarding warning) */
    long warn;                /* warn x hours before when; if omitted: 4 hours */
    int nocron;
    struct item *next;
} item_t;

typedef enum {IGNORE=0, NOT_DORMANT, HAS_DATE, DORMANT, UNCLASSIFIED} Age;
typedef enum {FALSE, TRUE} Bool;
typedef enum {DEAD, CLOSE, NORMAL, ROTTING} Classification;
typedef enum {NOIN=0, REGEXF, REGEX, SINGLE, DIRECT, INVALID} Input;
typedef enum {NONE, PRETTY, RAW, SPECIFIC, CRON_DC, CRON_D} Printing;

typedef struct gitodoc_settings {
    char data_dir[PATH_MAX];  /* GITODOC_DATA */
    char editor[NAME_MAX];    /* GITODOC_EDITOR, EDITOR */
    Bool important;           /* GITODOC_SHOW_UNIMPORTANT */
    Printing how_to_print;    /* Style and what to print */
    long arguments;           /* min 32-bit integer to store 4 8-bit integer */
} settings;

typedef struct input_data {
    char value[LEN_DATA];   /* optarg */
    Input input_type;       /* What kind of input */
} opt_input_t;

char *strptime(const char *s, const char *format, struct tm *tm);

int build_filepath(char *, char *, char *);
int check_for_duplicates(item_t *, char *);
int check_if_gitodoc_item(char *);
int get_data_dir(char *);
void get_editor(char *);
Bool get_important();
time_t get_localtime(char *);
item_t * get_pos_to_append(item_t *);
Input input_mapping(char *);
int str_to_long(char *, long *);
void pattern_matching(item_t *, FILE *);
int search_pattern(char *, char *);
int search_file(FILE *, char *);
void swap_elements(item_t **, item_t **, item_t **, item_t **);
item_t * sort_items(item_t *);
void trim_unnecessary(char *);
double time_delta(time_t);
Classification time_status(item_t *);

#endif
