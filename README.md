# gitodoc(lone)

A trial to replicate [gitodo](http://www.uninformativ.de/git/gitodo/file/README.html) in C instead of Ruby.
Doing it just for the cause to learn a bit more about programming in C.
I needed a project and since I kinda "dislike" the fact, that I only have ruby installed because of gitodo...  
Well not really bothering, as a Ruby Installation isn't requiring that amount of space on a disk, and I really like gitodo.  
But it makes a good reason to start something :D

## What is it?
gitodoc is a personal task manager for your command line.
Here's how you should think about the program:
* An important task comes up, but you don't have to do it right now.
* You write that task down and throw it on a big pile: You create a new gitodoc item.
* While doing so, you tell gitodoc how important your task is. Does it have a deadline? Is it more important than other tasks? Or is it totally unimportant and you don't want it to show up for the next couple of months?
* gitodoc can then sort your tasks for you and what you do with that information is up to you.

It also offers some options for scripting.
You could setup `cron jobs` or `systemd timer` which sends you e-mails about upcoming tasks and you can integrate it with bars like the one of dwm.

gitodoc can be used with a little tool called `highcal`:
It's much like cal(1), but it uses colors and can highlight days.  
There it either a version written in [Ruby](http://www.uninformativ.de/git/gitodo/file/highcal.html) or in [C](http://www.uninformativ.de/git/highcal/file/README.html)

## Differences with gitodo

Main difference is caused by the usage of getopt for option parsing.
It only allows one argument per option, so multiple assignments aren't possible.
For example, if more than one item should be deleted, the deletion option needs to supplied twice, as regex isn't allowed for write operations.  
Another difference are optional arguments for options.
Those need to come directly after the option with no whitespace allowed inbetween.
Example: `-n3` is good, `-n 3` is not.
See [this](https://stackoverflow.com/questions/26431682/why-cant-i-have-a-space-between-option-and-optional-argument-using-getopt) for an explanation.  

Furthermore with `Ruby` it made no difference if it was called as an argument or an option.
Either variants of `--list` like `list` or in shortform `l` were working.  

The raw output (`--raw/-r`) dropped the column about timetype.
Not really clear what it meant and seemed to be there for compatibility reasons.

There are currently no language files for editors included, although it would be trivial to port the one for `vi/m`.

And finally it should be noted that there may be differences in usage because of partial or missing implementations of various features from `gitodo`.

## Dependencies and Installation
`gitodoc` is written in C and therefore requires a respective toolchain.
It can be compiled with either [GCC](https://gcc.gnu.org/) or [Clang/LLVM](https://clang.llvm.org/) if the following requirements are satisfied:

 * glibc (2.25+)
 * libgit2 (0.20.0+)
 * pcre2 (10.40+)

For ease of compilation one can either use `make` (via the `Makefile`) or `Meson`.

### Makefile procedure:
```shell
$ cd path/to/src/
$ make
```
The `Makefile` doesn't have a `install` routine, therefore the classical `make install` won't work.
After it successfully compiled the files can be placed where one wants them to be.

### Meson procedure:
```shell
$ meson setup --prefix=/usr --buildtype=plain [source] builddir
$ meson compile -C builddir
```
and optionally
```shell
# meson install -C builddir --destdir "$pkgdir"
```

## Running
Before you're using `gitodoc` for the very first time, you must initialize your TODO repository:
```shell
$ cd "$XDG_DATA_HOME"
$ mkdir gitodoc.items
$ git init gitodoc.items
```
As with typical `git repositories` this can be shared with other machines.

After the prerequisit is satiesfied simply run the program on your terminal of choice and from any directory:
```shell 
$ gitodoc
```
Refer to the manpage for all options.
The manpage also explains how you can combine `gitodoc` with `highcal`.

## ToDo

Previously this section was used to inform about features still not reimplemented.
Now it tries to list the stuff where `gitodoc` differs and should be adjusted.
* Restrict regular expressions (`-b/--body` searches whole file, the rest only subject)
* Special identifiers for options (dead, close, today, week)
* Write operations capable of regex
* Automatic maintenance (to recognize manually emptied files)?
* Language files for editors?
* Rework usage of getopt
    * optional arguments are kinda frowned upon
    * switch to something different? Like [cargs](https://github.com/likle/cargs)?

And in general one can get an overview from the issues as I try to keep them updated.

## Bugs and such
As I'm not a programmer by profession, but interest, there will be parts in the code where others will scratch their head.  
Or find bugs.  
Reports on such misbehavings and commentary on what could be done better are greatly appreciated and welcome.
