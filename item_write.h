/*
 * Functions needed for writing files
 */

#ifndef ITEM_WRITE_H
#define ITEM_WRITE_H

#include <stdio.h>
#include <string.h>
#include <sys/random.h>

#include "utilities.h"

#define LEN_CMD NAME_MAX + LEN_FNAME

int delete_item(opt_input_t);
int edit_item(settings, char *);
int new_item(settings, char *);

#endif
