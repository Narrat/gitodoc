/*
 * Utility functions that are needed in one or more places
 */

#include "utilities.h"


int build_filepath(char *file_path, char *wd, char *file_name)
{
    /* Construct file path for the -f option */
    char *position;
    size_t dsize = PATH_MAX;

    for ( int stage = 1; stage <= 3; stage++ ) {
        switch (stage) {
            case 1: /* Working directory */
                position = memccpy(file_path, wd, '\0', dsize);
                break;
            case 2: /* Path delimeter */
                position = memccpy(position - 1, "/", '\0', dsize);
                break;
            case 3: /* File name */
                position = memccpy(position - 1, file_name, '\0', dsize);
                break;
        }
        if ( position != NULL ) {
            dsize = PATH_MAX - (size_t)(position - file_path - 1);
        } else {
            /* If there is no NULL-Byte then something went wrong. No attempt at recovering */
            fprintf(stderr, "build_filepath: filepath too long\n");
            return -1;
        }
    }

    return 0;
}

int check_for_duplicates(item_t *first_item, char *id_to_check)
{
    item_t *current_item = NULL;
    int ret = 0;

    current_item = first_item;
    while ( current_item != NULL ) {
        /* Check if file_id is unset (go to the freshly initialized item_t) */
        if ( strlen(current_item->file_id) != LEN_FNAME-1 ) {
            return 0;
        }

        ret = strncmp(id_to_check, current_item->file_id, LEN_FNAME-1);
        if ( ret == 0 ) {
            /* Equal strings */
            return -2;
        } else {
            current_item = current_item->next;
        }
    }

    return 0;
}

int check_if_gitodoc_item(char *search) {
    /*
     * Use pattern_matching() to decide if the cli argument is a gitodoc item file
     */
    if ( ( search_pattern(search, "^i?[0-9]{1,4}$")) >= 0 ) {
        /* False positives: i014 (should only work without the i) */
        return 0;
    }
    return -1;
}

int get_data_dir(char *path)
{
    char *env_path = NULL;
    char fallback_path[] = "/.local/share/gitodoc.items/";
    char *position = NULL;
    size_t dsize = PATH_MAX, offset = 0;

    /* Check some environment vars for the gitodoc dir */
    for ( int stage = 1; strlen(path) == 0; stage++ ) {
        switch (stage) {
            case 1: /* GITODOC_DATA */
                env_path = getenv("GITODOC_DATA");
                break;
            case 2: /* XDG_DATA_HOME */
                env_path = getenv("XDG_DATA_HOME");
                offset = 13;
                break;
            case 3: /* HOME + fallback path */
                env_path = getenv("HOME");
                offset = 0;
                break;
            case 4: /* Current dir */
                env_path = fallback_path+13;
                break;
        }
        if ( env_path != NULL) {
            position = memccpy(path, env_path, '\0', dsize);
        }
        if ( position != NULL ) {
            dsize = PATH_MAX - (size_t)(position - path - 1);
            if (stage == 2 || stage == 3) { /* Add missing path information for {,XDG_DATA_}HOME */
                position = memccpy(position - 1, fallback_path+offset, '\0', dsize);
            }
            if ( position == NULL ) {
                fprintf(stderr, "get_data_dir: filepath too long\n");
                return -1;
            }
        }
    }

    /* The path needs to end with / */
    if (*(position-2) != '/' ) {
        memccpy(position - 1, "/", '\0', dsize);
    }

    return 0;
}

void get_editor(char *editor_name)
{
    char *env_content = NULL;
    char fallback[] = "vi";
    void *pos = NULL;

    /* Check some environment vars for the preferred editor (length check due to either NULL or string terminator) */
    for ( int stage = 1; strlen(editor_name) <= 1; stage++ ) {
        switch (stage) {
            case 1: /* GITODOC_EDITOR */
                env_content = getenv("GITODOC_EDITOR");
                break;
            case 2: /* EDITOR */
                env_content = getenv("EDITOR");
                break;
            case 3: /* Fallback */
                env_content = fallback;
                break;
        }
        if ( env_content != NULL) {
            pos = memccpy(editor_name, env_content, '\0', NAME_MAX);
        }
    }

    /* If there is no NULL-Byte default to fallback */
    if ( pos == NULL ) {
        fprintf(stderr, "get_editor: Couldn't safely get chosen editor. Falling back to default\n");
        editor_name = fallback;
    }
}

Bool get_important()
{
    if ( getenv("GITODOC_SHOW_UNIMPORTANT") != NULL ) {
        return FALSE;
    } else {
        return TRUE;
    }
}

time_t get_localtime(char *saved_date)
{
    /*
     * Convert the date string into the calendar time
     * (time elapsed since the Epoch (00:00:00 on 01.01.1970, (UTC))
     */
    time_t mytime = time(NULL);
    struct tm curr_time = *localtime(&mytime);
    struct tm saved_d;

    strptime(saved_date, "%Y-%m-%d %H:%M:%S", &saved_d);

    /* Check for sane values. Incomplete strings will get garbage values */
    // Still to filter garbage values that would make sense
    if ( saved_d.tm_year < 0 || saved_d.tm_year > 137) {
        saved_d.tm_year = 137;
    }
    if ( saved_d.tm_mon < 0 || saved_d.tm_mon > 11 ) {
        saved_d.tm_mon = 0; // +1 for the actual month
    }
    if ( saved_d.tm_mday < 1 || saved_d.tm_mday > 31) {
        saved_d.tm_mday = 1;
    }
    if ( saved_d.tm_hour < 0 || saved_d.tm_hour > 24) {
        saved_d.tm_hour = 0;
    }
    if ( saved_d.tm_min < 0 || saved_d.tm_min > 60) {
        saved_d.tm_min = 0;
    }
    if ( saved_d.tm_sec < 0 || saved_d.tm_sec > 60) {
        saved_d.tm_sec = 0;
    }

    /* Set DST flag */
    saved_d.tm_isdst = curr_time.tm_isdst;

    return mktime(&saved_d);
}

item_t * get_pos_to_append(item_t *first_item)
{
    item_t *current_item = NULL;

    if (first_item == NULL) {
        first_item = malloc(sizeof(item_t));
        if (first_item == NULL) {
            fprintf(stderr, "Error allocating memory\n");
            return NULL;
        }
        return first_item;
    } else {
        current_item = first_item;
        while ( current_item->next != NULL ) {
            current_item = current_item->next;
        }

        current_item->next = malloc(sizeof(item_t));
        if (current_item->next == NULL) {
            fprintf(stderr, "Error allocating memory\n");
            return NULL;
        }

        return current_item->next;
    }
}

Input input_mapping(char *input)
{
    /*
     * If the input is undetermined.
     * Could either be no input (for optional arguments), specific file id (SINGLE)
     * or a regex term for the subject/what line.
     * Fully body regex and input from file is determined, therefore no need to cover those cases here.
     */
    int err = 0;
    long id = 0;

    if ( strlen(input) == 0 ) {
        return NOIN;
    }

    /* Regex is only allowed in this place with the identifier :/ */
    if ( strncmp(input, ":/", 2) == 0 ) {
        memmove(input, input+2, strlen(input));
        return REGEX;
    }

    /* Check for file name/ID */
    if ( strncmp(input, "i", 1) != 0 ) {
        err = str_to_long(input, &id);
    } else {
        err = str_to_long(input+1, &id);
    }
    if ( err != 0 ) {
        return INVALID;
    }
    if ( id >= 0 && id <= 9999 ) {
        snprintf(input, LEN_FNAME, "i%04ld", id);
        return SINGLE;
    }
    return INVALID;
}

int str_to_long(char *str, long *nmbr)
{
    char *remainder = NULL;

    trim_unnecessary(str);
    errno = 0;
    *nmbr = strtol(str, &remainder, 10);
    if ( errno == ERANGE ) {
        fprintf(stderr, "str_to_long: Input is out of range\n");
        return -1;
    }
    if ( remainder == str ) {
        fprintf(stderr, "str_to_long: Input couldn't be converted into a valid ID (%s)\n", remainder);
        return -1;
    }
    if ( *remainder != '\0' ) {
        fprintf(stderr, "str_to_long: Input contained non-numerical input (%s)\n", remainder);
    }
    return 0;
}

void pattern_matching(item_t *curr_item, FILE *open_file)
{
    char buffer[LEN_DATA] = "";
    int keyword_len = 5, err = 0;
    void *pos = NULL;

    /* Set defaults */
    err = str_to_long(curr_item->file_id+1, &curr_item->id);
    if ( err != 0 ) {
        fprintf(stderr, "pattern_matching: Something went wrong while converting the file name to an ID");
    }
    memccpy(curr_item->what, "<NO SUBJECT>", '\0', LEN_DATA);
    curr_item->prio = 0;
    memccpy(curr_item->when, "2037-01-01 00:00:00", '\0', LEN_DATE);
    curr_item->warn = 1;
    curr_item->nocron = 0;
    curr_item->next = NULL;

    /* Get values from file if they exist */
    while ( (fgets(buffer, LEN_DATA, open_file))!=NULL ) {
        if ( (keyword_len = search_pattern(buffer, "^[s,w].*?t:")) >= 0 ) {
            /* what & subject */
            memccpy(curr_item->what, buffer+keyword_len, '\0', LEN_DATA);
        } else if ( (keyword_len = search_pattern(buffer, "^prio:")) >= 0 ) {
            /* prio */
            err = str_to_long(buffer+keyword_len, &curr_item->prio);
            if ( err != 0 ) {
                fprintf(stderr, "pattern_matching: Something went wrong while parsing the priority\n");
            }
        } else if ( (keyword_len = search_pattern(buffer, "^[d,w][e,h].[d,n]:")) >= 0 ) {
            /* when & dead */
            pos = memccpy(curr_item->when, buffer+keyword_len, '\0', LEN_DATE);
            if ( pos == NULL ) {
                curr_item->when[LEN_DATE-1] = '\0';
            }
        } else if ( (keyword_len = search_pattern(buffer, "^wa.*?:")) >= 0 ) {
            /* warn */
            err = str_to_long(buffer+keyword_len, &curr_item->warn);
            if ( err != 0 ) {
                fprintf(stderr, "pattern_matching: Something went wrong while parsing the warn time\n");
            }
        } else if ( search_pattern(buffer, "^n.*?n:") >= 0 ) {
            /* nocron */
            curr_item->nocron = 1;
        }
    }

    /*
     * Finishing touch:
     * > Fill localtime field
     * > Strip leading Whitespaces and trailing newlines
     */
    curr_item->when_local = get_localtime(curr_item->when);
    trim_unnecessary(curr_item->what);
    trim_unnecessary(curr_item->when);
}

int search_pattern(char *to_search, char *term)
{
    /* PCRE related vars */
    pcre2_code *re;
    /* PCRE2_SPTR is a pointer to unsigned code units of the appropriate width (8, 16, or 32 bits). */
    PCRE2_SPTR pattern, subject;
    int errornumber, rc, substring_length=-1;
    PCRE2_SIZE erroroffset;
    PCRE2_SIZE *ovector;
    size_t subject_length;
    pcre2_match_data *match_data;

    /*
     * As pattern and subject are char arguments, they can be straightforwardly
     * cast to PCRE2_SPTR as we are working in 8-bit code units.
     */
    pattern = (PCRE2_SPTR)term;

    /* Compilation of re pattern */
    re = pcre2_compile(
      pattern,               /* the pattern */
      PCRE2_ZERO_TERMINATED, /* indicates pattern is zero-terminated */
      0,                     /* default options */
      &errornumber,          /* for error number */
      &erroroffset,          /* for error offset */
      NULL);                 /* use default compile context */

    /* Compilation failed: print the error message and exit. */
    if ( re == NULL ) {
        PCRE2_UCHAR buffer[256];
        pcre2_get_error_message(errornumber, buffer, sizeof(buffer));
        printf("PCRE2 compilation failed at offset %d: %s\n", (int)erroroffset, buffer);
        return substring_length;
    }

    /* Search supplied line for term */
    subject = (PCRE2_SPTR)to_search;
    subject_length = strlen((char *)to_search);

    /*
     * Using this function ensures that the block is exactly the right size for
     * the number of capturing parentheses in the pattern.
     */
    match_data = pcre2_match_data_create_from_pattern(re, NULL);
    /* And now do just ONE match */
    rc = pcre2_match(
      re,                   /* the compiled pattern */
      subject,              /* the subject string */
      subject_length,       /* the length of the subject */
      0,                    /* start at offset 0 in the subject */
      0,                    /* default options */
      match_data,           /* block for storing the result */
      NULL);                /* use default match context */

    /* Matching failed: handle error cases */
    if ( rc < 0 ) {
        switch (rc) {
            case PCRE2_ERROR_NOMATCH:
                break;
            /* Handle other special cases */
            default:
                fprintf(stderr, "search_pattern: Matching error %d\n", rc);
                break;
        }
    } else {
        /* Match succeeded: Prepare length as return value */
        ovector = pcre2_get_ovector_pointer(match_data);
        if (rc == 0) {
            fprintf(stderr, "search_pattern: ovector was not big enough for all the captured substrings\n");
        }
        substring_length = (int)ovector[1] - (int)ovector[0];
    }
    pcre2_match_data_free(match_data);
    pcre2_code_free(re);

    return substring_length;
}

int search_file(FILE *open_file, char *term)
{
    char *line = NULL;
    size_t len=0;
    int res=0;

    /* Search line for term */
    while ( getline(&line, &len, open_file) != -1 ) {
        res = search_pattern(line, term);
        if ( res > 0 ) {
            return 0;
        }
    }
    /* No match */
    return 1;
}

void swap_elements(item_t **current, item_t **previous, item_t **next, item_t **first_item)
{
    /* Swap two elements of the single linked list */
    if (*current == *first_item){
        /* Special case: Beginning of list needs to be exchanged */
        *first_item = *next;
    } else {
        (*previous)->next = *next;
    }
    (*current)->next = (*next)->next;
    (*next)->next = *current;

    *previous = *next;
    *next = (*current)->next;
}

item_t * sort_items(item_t *first_item)
{
    /* Don't try to sort a NULL pointer */
    if ( first_item == NULL ) {
        return NULL;
    }

    /* For the time being: Bubble Sort */
    item_t *current = first_item;
    item_t *next = current->next;
    item_t *previous = NULL;
    Bool changed = TRUE;

    while (changed != FALSE) {
        /* Asume nothing changed */
        changed = FALSE;

        while (current->next != NULL) {
            /*
             * Something needs to be adjusted if next->prio or next->when_local
             * is smaller than current
             */
            if (next->prio < current->prio) {
                swap_elements(&current, &previous, &next, &first_item);
                changed = TRUE;
            } else if ( (next->prio == current->prio) &&
                        (next->when_local < current->when_local) ) {
                /* Sort existing date entries from low to high */
                swap_elements(&current, &previous, &next, &first_item);
                changed = TRUE;
            } else { /* Nothing needs to be adjusted */
                previous = current;
                current = current->next;
                next = current->next;
            }
        }

        /* Preparations for the next round: Reset to the beginning */
        current = first_item;
        next = current->next;
        previous = NULL;
    }

    return first_item;
}

void trim_unnecessary(char *to_trim)
{
    /* Check for leading whitespace */
    if (isspace(*(to_trim))) {
        memmove(to_trim, to_trim+1, strlen(to_trim));
    }

    /* Remove first occuring LF, CR, CRLF, LFCR, ... */
    to_trim[strcspn(to_trim, "\r\n")] = '\0';
}

double time_delta(time_t saved_date)
{
    /*
     * Compare two UNIX time stamps and return the diff in h
     * Positive: There is time left
     * Negative: Past the expiration
     */
    time_t mytime;
    mytime = time(NULL);

    return ( difftime( saved_date, mytime ) / (double)3600 );
}

Classification time_status(item_t *item)
{
    double remaining;

    remaining = time_delta(item->when_local);
    if ( (int)remaining >= item->warn ) {
        return NORMAL;
    } else if ( (int)remaining < item->warn && remaining > 0 ) {
        return CLOSE;
    } else {
        return DEAD;
    }
}
