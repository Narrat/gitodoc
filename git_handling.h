/*
 * Functions for managing the gitodoc repository
 */

#ifndef GIT_HANDLING_H
#define GIT_HANDLING_H

#include <stdio.h>
#include <unistd.h>
#include <git2.h>

#include "utilities.h"


#define LEN_CMD_AGE 46 /* git cmd ("git log '--pretty=format:%ct' -n 1 --") + filename */
#define LEN_MSGS 100
#define REPO "./.git"


int gethostname(char *name, size_t len);

void check_error(int, const char *);
int check_for_repo(char *);
void check_age(Age *, char *, long);
void commit_changes();
int git_get_file_oid(git_buf *, git_repository *, char *, git_commit *);

#endif
