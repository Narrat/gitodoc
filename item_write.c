/*
 * Functions needed for writing files
 */

#include "item_write.h"


int delete_item(opt_input_t id_to_del)
{
    int error=0;

    if ( id_to_del.input_type != SINGLE ) {
        fprintf(stderr, "delete_item: '%s' isn't a valid file name\n", id_to_del.value);
        return -1;
    }

    printf("Deleting %s\n", id_to_del.value);
    error = remove(id_to_del.value);
    if ( error != 0 ) {
        fprintf(stderr, "delete_item: Couldn't remove %s\n", id_to_del.value);
        return -1;
    }
    return 0;
}

int edit_item(settings gitodoc, char *id_to_edit)
{
    /* Check if file exists, open or fail silently */
    char command[LEN_CMD]="";
    int error=0;
    Input in_type=NOIN;
    FILE *fp;

    /* Check if file exists */
    in_type = input_mapping(id_to_edit);
    if ( in_type != SINGLE ) {
        fprintf(stderr, "edit_item: '%s' couldn't be mapped to a valid file name\n", id_to_edit);
        return -1;
    }

    fp = fopen(id_to_edit, "r");
    if (fp != NULL) {
        fclose(fp);
    } else {
        fprintf(stderr, "edit_item: Error opening file '%s': %s\n", id_to_edit, strerror(errno));
        return -1;
    }

    /* Open file with editor */
    snprintf(command, LEN_CMD, "%s %s", gitodoc.editor, id_to_edit);
    error = system(command);
    if (error == -1) {
        fprintf(stderr, "edit_item: Error executing command '%s': %s\n", command, strerror(errno));
        return -1;
    }

    return 0;
}

int new_item(settings gitodoc, char *file_to_include)
{
    char command[LEN_CMD]="", file_name[LEN_FNAME]="";
    int error=0;
    size_t random_val = 0;
    ssize_t ret=0, chr, size=0;
    FILE *fp, *fpi;
    Bool found_unique=FALSE;

    do {
        /* Generate four-digit random number and create file name */
        ret = getrandom(&random_val, sizeof(size_t), 0);
        if ( ret != sizeof(size_t) ) {
            fprintf(stderr, "getrandom() returned %d: ", (int)ret);
        }
        snprintf(file_name, LEN_FNAME, "i%04zd", random_val % 10000);

        /* Check for possible existing file */
        fp = fopen(file_name, "r");
        if ( fp != NULL ) {
            fclose(fp);
        } else {
            found_unique = TRUE;
        }
    } while ( found_unique == FALSE );

    /*
     * Create item file + mandatory keywords, if no file to include is given.
     * Else the contents of the file to include is copied to the new item.
     * If the file to include cannot be opened continue like normal.
     */
    fp = fopen(file_name, "w");
    if ( fp == NULL ) {
        fprintf(stderr, "new_item: Error opening file '%s': %s\n", file_name, strerror(errno));
        return -1;
    }
    if ( file_to_include == NULL ) {
        fputs("what: ", fp);
    } else {
        fpi = fopen(file_to_include, "r");
        if ( fpi != NULL ) {
            while ( (chr = fgetc(fpi)) != EOF ) {
                fputc((char)chr, fp);
            }
            fclose(fpi);
        } else {
            fprintf(stderr, "new_item: Error opening file '%s': %s\n", file_to_include, strerror(errno));
            fputs("what: ", fp);
        }
    }
    fclose(fp);

    /* Open file with editor */
    snprintf(command, LEN_CMD, "%s %s", gitodoc.editor, file_name);
    error = system(command);
    if ( error == -1 ) {
        fprintf(stderr, "new_item: Error executing command '%s': %s\n", command, strerror(errno));
        return -1;
    }

    /*
     * Check if the new file has any size
     * Doesn't cover for whitespace only cases
     */
    fp = fopen(file_name, "r");
    if ( fp == NULL ) {
        fprintf(stderr, "new_item: Error opening file '%s': %s\n", file_name, strerror(errno));
        return -1;
    }
    fseek(fp, 0L, SEEK_END);
    size = ftell(fp);
    fclose(fp);

    if ( size <= 1 ) {
        error = remove(file_name);
        if ( error != 0 ) {
            fprintf(stderr, "new_item: Couldn't remove empty file %s\n", file_name);
        }
        return -1;
    }
    return 0;
}
